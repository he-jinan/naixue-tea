const express=require('express');
const http = require('http')
const app=express();
app.listen(3000,()=>{
  console.log('3000♥ ♥ ♥接口服务器启动成功')
})
app.all('*',(req,res,next)=>{
  res.header('Access-Control-Allow-Origin','*')
  next()
})
//托管静态资源
app.use(express.static('./upload'))
//post传参解析
app.use(express.urlencoded({
  extended:false
}))
//引入路由
const userRouter=require('./router/user')
app.use('/user',userRouter)
const homeRouter=require('./router/home')
app.use('/home',homeRouter)
const teaRouter=require('./router/tea')
app.use('/tea',teaRouter)
const adminRouter=require('./router/admin')
app.use('/admin',adminRouter)

//错误处理中间件
app.use((err,req,res,next)=>{
  console.log(err)
  res.status(500).send({
    "code":500,
    "msg":"服务器端错误"
  })
})