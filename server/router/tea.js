const express = require("express")
const pool = require("./pool")
const router = express.Router()
//商品种类接口
// url: http://127.0.0.1:3000/tea/type
router.get('/type',(req,res,next)=>{
  let obj=req.query
  console.log(obj)
  pool.query('select * from nx_tea_family',[obj],(err,data)=>{
    if(err) {
      next(err)
      return
    }
    console.log(res)
    if(data.length!=0){
      res.send({
          "code":1,
    "msg":"查找成功",
    "data":data
      })
  }else{
      res.send({
          "code":0,
    "msg":"查找失败",
      })
  }
  })
})
//商品页店铺推荐
// url: http://127.0.0.1:3000/tea/recommend
router.get('/recommend',(req,res,next)=>{
  let obj=req.query
  console.log(obj)
  pool.query('select * from nx_tea_rec',[obj],(err,data)=>{
    if(err) {
      next(err)
      return
    }
    console.log(res)
    if(data.length!=0){
      res.send({
          "code":1,
    "msg":"查找成功",
    "data":data
      })
  }else{
      res.send({
          "code":0,
    "msg":"查找失败",
      })
  }
  })
})
//商品列表接口
// url: http://127.0.0.1:3000/tea/list
//get ?在url里传参   familyid
//访问数据表nx_tea  family_id
// select lid,family_id,names,tag,pic,price,details from nx_tea right outer join nx_tea_family on fid=family_id where fid=?
router.get("/list", (req, res, next) => {
    //检查客户端是否成功连接服务器端
    console.log('11行查询某商品接口连接成功')
    let obj = req.query
    console.log(obj)
    pool.query("select * from nx_tea right outer join nx_tea_family on fid=family_id where fid=?", [obj.fid], (err, data) => {
      if (err) {
        next(err)
        return
      }
      //console.log(data)
      //判断data是不是空数组
      if (!data.length) {
        res.send({
          code: 0,
          msg: "没查到该商品信息",
        })
      } else {
        res.send({
          code: 1,
          msg: "已返回查询到的商品数据",
          data: data,
        })
      }
    })
  })

// 商品详情页接口
// url: http://127.0.0.1:3000/tea/query
//get ?在url里传参   lid
//访问数据表nx_tea  lid
router.get("/query", (req, res, next) => {
  //检查客户端是否成功连接服务器端
  //console.log('42行查询某商品接口连接成功')
  let obj = req.query
  //console.log(obj)
  pool.query("select * from nx_tea where lid=?", [obj.lid], (err, data) => {
    if (err) {
      next(err)
      return
    }
    //console.log(data)
    //判断data是不是空数组
    if (!data.length) {
      res.send({
        code: 0,
        msg: "没查到该商品信息",
      })
    } else {
      res.send({
        code: 1,
        msg: "已返回查询到的商品数据",
        data: data,
      })
    }
  })
})
//商品页消息栏
//http://127.0.0.1:3000/tea/notice
router.get('/notice',(req,res,next)=>{
  let obj=req.query
  console.log(obj)
  pool.query('select * from nx_notice',[obj],(err,data)=>{
      if(err){
          next(err)
          return
      }
      console.log(res)
      if(data.length!=0){
          res.send({
              "code":1,
        "msg":"查找成功",
        "data":data
          })
      }else{
          res.send({
              "code":0,
        "msg":"查找失败",
          })
      }
  })
})
module.exports=router


