const express=require('express')
const pool=require('./pool')
const router=express.Router()
//首页轮播图接口
//http://127.0.0.1:3000/home/swipe
router.get('/swipe',(req,res,next)=>{
    let obj=req.query
    console.log(obj)
    pool.query('select * from nx_swipe',[obj],(err,data)=>{
        if(err){
            next(err)
            return
        }
        console.log(res)
        if(data.length!=0){
            res.send({
                "code":1,
          "msg":"查找成功",
          "data":data
            })
        }else{
            res.send({
                "code":0,
          "msg":"查找失败",
            })
        }
    })
})
//活动图片接口
//http://127.0.0.1:3000/home/thing
router.get('/thing',(req,res,next)=>{
    let obj=req.query
    console.log(obj)
    pool.query('select * from nx_thing',[obj],(err,data)=>{
        if(err){
            next(err)
            return
        }
        console.log(res)
        if(data.length!=0){
            res.send({
                "code":1,
          "msg":"查找成功",
          "data":data
            })
        }else{
            res.send({
                "code":0,
          "msg":"查找失败",
            })
        }
    })
})
//首页底部会员新鲜事接口
//http://127.0.0.1:3000/home/new
router.get('/new',(req,res,next)=>{
    let obj=req.query
    console.log(obj)
    pool.query('select * from nx_new',[obj],(err,data)=>{
        if(err){
            next(err)
            return
        }
        console.log(res)
        if(data.length!=0){
            res.send({
                "code":1,
          "msg":"查找成功",
          "data":data
            })
        }else{
            res.send({
                "code":0,
          "msg":"查找失败",
            })
        }
    })
})
//储蓄卡接口
//http://127.0.0.1:3000/home/balance
router.get('/balance',(req,res,next)=>{
    let obj=req.query
    console.log(obj)
    pool.query('select * from nx_balance',[obj],(err,data)=>{
        if(err){
            next(err)
            return
        }
        console.log(res)
        if(data.length!=0){
            res.send({
                "code":1,
          "msg":"查找成功",
          "data":data
            })
        }else{
            res.send({
                "code":0,
          "msg":"查找失败",
            })
        }
    })
})
module.exports=router