const express=require('express')
const pool=require('./pool')
const router=express.Router()
//用户更改性别接口
//http://127.0.0.1:3000/user/gender
//nx_user数据表   unames gender 
router.post('/gender',(req,res,next)=>{
  console.log('用户更改性别接口连接成功');
  let obj=req.body
  console.log(obj);
  let sql=`update nx_user set gender=${obj.gender} where unames="${obj.unames}";`
  pool.query(sql,(err,data)=>{
    if(err){next(err);return;}
    //检查数据库改动条数
    if(!data.affectedRows){
      res.send({
        "code":0,
        "msg":"修改失败"
      })
    }else{
      res.send({
        "code":1,
        "msg":"修改成功",
        "gender":obj.gender
      })
    }
  })
})


//用户登录接口
//http://127.0.0.1:3000/user/login
//nx_user数据表    unames upwd
router.post('/login',(req,res,next)=>{
    //console.log('7行检查登录接口连接成功')
    let obj=req.body
    //console.log(obj)
    pool.query('select * from nx_user where upwd=? and unames=?',[obj.upwd,obj.unames],(err,data)=>{
        console.log(data)
        if(err){next(err);return;}
        if(data.length==1){
          res.send({
            "code":1,
            "msg":"登录成功",
            "data":data[0]
          })
        }else{
          res.send({
              "code":0,
              "msg":"用户名或密码错误,登录失败"
          })
        }
      })
})
router.post('/avatar',(req,res,next)=>{
  let obj=req.body
  pool.query('update nx_user set avatar=? where uid=?',[obj.avatar,obj.uid],(err,data)=>{
    console.log(data)
    if(err){next(err);return;}
    console.log(res)
    if(res.affectedRows===0){
	    res.send({code:501,msg:'修改失败'});
	 }else{
	    res.send({code:200,msg:'修改成功'});
	 }
  })
})

//用户注册接口
//http://127.0.0.1:3000/user/reg
//nx_user数据表    unames upwd phone
router.post('/reg',(req,res,next)=>{
  //console.log('29行检查登录接口连接成功')
  let obj=req.body
  console.log(obj)
  pool.query(`insert into nx_user set ?`,[obj],(err,data)=>{
      console.log(data)
      if(err){next(err);return;}
        res.send({
          "code":1,
          "msg":"注册成功",
          "data":obj
      })
    })
})   

//用户修改密码接口 /editpwd    url: http://127.0.0.1:3000/user/editpwd  
//post  约定传参  
//修改数据需要一个旧值  原密码:oldpwd,用户名unames 可以修改的新值:newpwd
//访问数据表nx_user  
router.post('/editpwd',(req,res,next)=>{
  //检查客户端是否成功连接服务器端
  console.log('66行用户修改密码接口连接成功')
  let obj=req.body
  //console.log(obj)
  let sql=`update nx_user set upwd="${obj.newpwd}" where unames="${obj.unames}" and upwd="${obj.oldpwd}";`
  pool.query(sql,(err,data)=>{
    if(err){next(err);return;}
    //检查数据库改动条数
    if(!data.affectedRows){
      res.send({
        "code":0,
        "msg":"修改失败"
      })
    }else{
      res.send({
        "code":1,
        "msg":"修改成功"
      })
    }
  })
})

//用户修改手机号接口 /phone    url: http://127.0.0.1:3000/user/phone  
//post  约定传参  
//修改数据需要一个旧值  原密码:oldphone,用户名unames 可以修改的新值:newphone
//访问数据表nx_user  
router.post('/phone',(req,res,next)=>{
  //检查客户端是否成功连接服务器端
  console.log('93行用户修改密码接口连接成功')
  let obj=req.body
  //console.log(obj)
  let sql=`update nx_user set phone="${obj.newphone}" where unames="${obj.unames}" and phone="${obj.oldphone}";`
  pool.query(sql,(err,data)=>{
    if(err){next(err);return;}
    //检查数据库改动条数
    if(!data.affectedRows){
      res.send({
        "code":0,
        "msg":"手机号修改失败"
      })
    }else{
      res.send({
        "code":1,
        "msg":"手机号修改成功",
	    "data":obj.newphone
      })
    }
  })
})

//配置multer中间件
const multer = require('multer')
//创建存储方案：diskStorage （存储磁盘）
obj = multer.diskStorage({
  destination : function(req, file, cb){ //指定目录
    cb(null, 'upload/header')
  },
  filename : function (req, file, cb){ // 指定文件名
    // console.log(uuid.v1())
    // console.log(uuid.v4())
    let name = file.originalname
    // name:  abcd.jpg    xxxdfdd.zip
    let ext = name.substr(name.lastIndexOf('.'))
    cb(null, uuid.v4() + ext)
  }
})
const uploadTools = multer({
  storage : obj
})
const uuid = require('uuid')


//接收请求
router.post('/upload',
  uploadTools.array('uploadFile'), (req, res)=>{
    //console.log(req.files)
    //res.send('OK')
    //输出所有已上传文件的文件名
    let urls=[]
    req.files.forEach(item=>{
      console.log(item.filename)
      urls.push('http://127.0.0.1:3000/'+item.filename)
    })
    res.send(urls)
})
module.exports=router
