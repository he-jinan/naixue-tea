const express = require("express")
const pool = require("./pool")
const router = express.Router()

//管理员登录接口
//http://127.0.0.1:3000/admin/login
//nx_admin数据表    aname pwd
router.post('/login',(req,res,next)=>{
    //console.log('7行检查登录接口连接成功')
    let obj=req.body
    //console.log(obj)
    pool.query('select * from nx_admin where pwd=? and aname=?',[obj.pwd,obj.aname],(err,data)=>{
        console.log(data)
        if(err){next(err);return;}
        if(data.length==1){
          res.send({
            "code":1,
            "msg":"登录成功",
            "data":data[0]
          })
        }else{
          res.send({
              "code":0,
              "msg":"用户名或密码错误,登录失败"
          })
        }
      })
})

module.exports=router