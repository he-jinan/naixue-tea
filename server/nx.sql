set names utf8;
drop database if exists nx;
create database nx charset = utf8;
use nx;

#用户表
create table nx_user(
  uid int primary key not null auto_increment,
  unames varchar(32) not null unique,
  upwd varchar(32),
  phone varchar(16) not null,
  avatar varchar(128) default "default_head.jpg",
  gender int default 1, #1女 0男
  integral int default 0,
  balance int default 0,
  card int default 0,
  discount int default 0
) ENGINE = InnoDB charset = utf8; 

insert into nx_user(unames,upwd,phone) values
('tom','wydxb555','15201076723'),
('nancy','wydxb555','15201197440'),
('ball','wydxb666','15910779501'),
('dan','wydxb666','15910780895');

#商品分类表
create table nx_tea_family(
  fid int primary key auto_increment,
  fname varchar(32)
) ENGINE = InnoDB charset = utf8;

insert into nx_tea_family (fname) values
("超值好喝"),
("热饮推荐"),
("当季限定"),
("奈雪必点"),
("霸气鲜果茶"),
("宝藏鲜奶茶"),
("精品咖啡"),
("新中式点心"),
("面包吐司"),
("千层蛋糕"),
("罐头蛋糕");
#商品表 
create table nx_tea(
  lid int primary key auto_increment,
  family_id int,
  foreign key(family_id) references nx_tea_family(fid),
  names varchar(32),#奶茶名称
  subtit varchar(32),#可做方式
  tag varchar(64),#左上角标签
  pic varchar(128),#图片路径
  price decimal(6,2) not null,#价格
  costPrice decimal(6,2),#原价
  details varchar(512), #商品描述
  feeding varchar(256),#加料
  Sugar varchar(256),#糖度
  temperature varchar(256),#温度
  straw varchar(256)#吸管
) ENGINE = InnoDB charset = utf8;

INSERT INTO `nx_tea` (`lid`, `family_id`, `names`, `subtit`, `tag`, `pic`, `price`, `costPrice`, `details`, `feeding`, `Sugar`, `temperature`, `straw`) VALUES
(1, 1, '轻松金牡丹', '可做热饮', '热销', '1.png', '25.00', '38.00', '[650ml]严选质优好橙,饱满多汁的橙子,柠檬片搭配经典奈雪金奖茶茉莉初雪,畅饮一口,橙意满满,花香四溢,给你带来对橙汁的全新认知', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(2, 1, '芝士轻松金牡丹', '可做热饮', NULL, '2.png', '14.00', '20.00', '【650ml】,满满的葡萄肉感!选用优质巨峰葡萄,手工去皮去籽保留大颗果肉,搭配带兰桂花香的金观音茶底,酣畅淋漓', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(3, 1, '霸气鸭香柠檬', '可做热饮  手拎香水柠檬', '推荐', '3.png', '17.00', '25.00', '【650ml】优良培育的西柚与奈雪金奖茶茉莉初雪的奇妙碰撞,一秒打开味蕾,吃得到的果肉粒粒饱满,搭配花茶更有独特清甜', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(4, 3, '清心乌龙', '可做热饮', '热销', '5.png', '12.00', '18.00', '【500ml】优选高山茶园培育的乌龙茶叶,经过不低于8小时的低温慢泡,有效降低茶汤的茶涩感。入口甘甜，茶味清新，真正诠释了好茶才能做冷泡', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(5, 4, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(6, 2, '金色山脉宝藏茶', '可做热饮', '热销', '4.png', '13.00', '20.00', '【500ml】独具高山茶韵的金色山脉红茶茶底,冲入香滑牛奶,搭配清新椰奶冻与嚼劲十足黑糖珠珠,加盖醇香动物奶油顶,撒上酥脆碧根果脆', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(7, 5, '金色山脉宝藏茶', '可做热饮', '热销', '4.png', '13.00', '20.00', '【500ml】独具高山茶韵的金色山脉红茶茶底,冲入香滑牛奶,搭配清新椰奶冻与嚼劲十足黑糖珠珠,加盖醇香动物奶油顶,撒上酥脆碧根果脆', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(8, 6, '金色山脉宝藏茶', '可做热饮', '热销', '3.png', '13.00', '20.00', '【500ml】独具高山茶韵的金色山脉红茶茶底,冲入香滑牛奶,搭配清新椰奶冻与嚼劲十足黑糖珠珠,加盖醇香动物奶油顶,撒上酥脆碧根果脆', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(9, 7, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(10, 8, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(11, 9, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(12, 10, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>'),
(13, 11, '咸蛋黄嘟嘟', '可做热饮', NULL, '1.png', '17.00', '26.00', '【约240g】三层內馅,咸香蛋黄浓郁细腻,给厚芋泥注入灵魂,还有美味馋嘴松松,甜咸搭配一口上瘾。[过敏原提示:本品含小麦粉,蛋及奶制品,乳及乳制品]', '<div>燃爆菌￥4.00</div><div>0卡糖￥1.00</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>标准甜(推荐)</span></div></div><div>少甜</div><div>少少甜</div><div>无糖</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>常温(推荐)</span></div></div><div>少冰</div><div>多冰</div><div>加热</div>', '<div><div class=''img''><img src=''/imgs/proDetail.png''></div><div><span>PLA可降解吸管(推荐)</span></div></div><div>不使用吸管</div><div>可降解纸吸管</div>');

#商品推荐表,通过推荐的商品lid获取到数据
create table nx_tea_rec(
  rid int primary key auto_increment,
  l_lid int,#关联商品lid
  family_id int,
  names varchar(32),#奶茶名称
  pic varchar(256),
  price decimal(6,2) not null,#价格
  foreign key(l_lid) references nx_tea(lid)
) ENGINE = InnoDB charset = utf8;
#商品页推荐商品
insert into nx_tea_rec values
(null,2, 1, '芝士轻松金牡丹','2.png', '14.00'),
(null,3, 1, '霸气鸭香柠檬', '3.png', '17.00'),
(null,5, 4, '咸蛋黄嘟嘟', '1.png', '17.00');
#商品页消息栏
create table nx_notice(
  nid int primary key auto_increment,
  pic varchar(128),
  content varchar(256)
) ENGINE = InnoDB charset = utf8;
insert into nx_notice values
(null,"1.png","冬日限定:储值300立赠1杯"),
(null,"2.png","满59元赠虎年好运红包(结算勾选赠品)"),
(null,"3.png","共同防疫-减配送费");
#首页轮播图
create table nx_swipe(
  sid int primary key not null auto_increment,
  image varchar(128),
  title varchar(64)
) ENGINE = InnoDB charset = utf8; 
insert into nx_swipe values(null,"default_swiper01.jpg",'轮播图1');
insert into nx_swipe values(null,"default_swiper02.jpg",'轮播图2');
insert into nx_swipe values(null,"default_swiper03.jpg",'轮播图3');
insert into nx_swipe values(null,"default_swiper04.jpg",'轮播图4');
insert into nx_swipe values(null,"default_swiper05.jpg",'轮播图5');

#活动图片数据
create table nx_thing(
  tid int primary key not null auto_increment,
  image varchar(128),
  title varchar(64),
  subtit varchar(64)
) ENGINE = InnoDB charset = utf8;
insert into nx_thing values(null,"default_thing01.jpg",'奈雪的茶商城','好茶随行 美好常在');
insert into nx_thing values(null,"default_thing02.jpg",'储值有礼','储值300立赠1杯');
insert into nx_thing values(null,"default_thing03.jpg",'心意卡','送TA心意一起闹元宵');
#会员充值储蓄卡数据
create table nx_balance(
  bid int primary key not null auto_increment,
  icon varchar(128),
  title varchar(64),
  money int
) ENGINE = InnoDB charset = utf8;
insert into nx_balance values(null,"b1.png",'单品免费券x1',300);
insert into nx_balance values(null,"b2.png",'5元现金券x1',100);
insert into nx_balance values(null,"b3.png",'第二杯半价券x1',200);
insert into nx_balance values(null,"b4.png",'第二杯半价券x3',500);
#首页底部会员新鲜事
create table nx_new(
  nid int primary key not null auto_increment,
  image varchar(128),
  title varchar(64)
) ENGINE = InnoDB charset = utf8;
insert into nx_new values(null,"default_new01.jpg",'图片1');
insert into nx_new values(null,"default_new02.jpg",'图片2');
insert into nx_new values(null,"default_new03.jpg",'图片3');
insert into nx_new values(null,"default_new04.jpg",'图片4');

#管理员用户表
create table nx_admin(
  aid int primary key not null auto_increment,
  aname varchar(32) not null,
  pwd varchar(32),
  avatar varchar(128) default "default_head.jpg",
  address varchar(64) 
) ENGINE = InnoDB charset = utf8; 

insert into nx_admin(aname,pwd,address) values
('tom','woshiguanli','和平路店'),
('nancy','woshiguanli','鲁能城店'),
('ball','woshiguanli','一中心店'),
('dan','woshiguanli','大学城店');