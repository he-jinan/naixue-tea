import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //图片共享路径
    imgURL:'http://127.0.0.1:3000/header/',
    //首页轮播图路径
    swipeURL:'http://127.0.0.1:3000/banners/',
    //主页底部活动图片路径
    thingURL:'http://127.0.0.1:3000/thing_imgs/',
    // 首页会员新鲜事图片路径
    newURL:'http://127.0.0.1:3000/new_imgs/',
    //储蓄类型图片路径
    banURL:'http://127.0.0.1:3000/balance_card/',
    //商品图片路径
    shopURL:'http://127.0.0.1:3000/products_img/',
    //消息栏小图片路径
    noticeURL:'http://127.0.0.1:3000/notice_img/',
    //用于存储登录用户信息数据
    user:null,
    //服务器存储头像图片地址
    headImg:null,
    //存放首页会员新鲜事图片数据
    newImg:null,
    //存放轮播图数据
    swipeImg:null,
    // 存储商品数据
    shops:null,
    //存储用户选择的地址
    addr:null,
    //存储地位信息
    postion:null,
  },
  mutations: {
    updateUser(state,user){
      state.user=user
    },
    updateUphone(state,phone){
      state.user.data.phone=phone
    },
    updateA(state,avatar){
       state.user.data.avatar=avatar
    },
    updateHead(state,headImg){
      state.headImg=headImg
    },
    updateNew(state,newImg){
      state.newImg=newImg
    },
    //轮播图 
    updateSwipe(state,swipeImg){
      state.swipeImg=swipeImg
    },
    //商品数据
    updateShop(state,shops){
      state.shops=shops
    },
    //用户选中的地址
    updateAddr(state,addr){
      state.addr=addr
    },
    updatePos(state,postion){
      state.postion=postion
    }
  },
  actions: {
    //会员新鲜事路由共享
    //获取首页会员新鲜事图片数据
    getNew(store){
      const url='home/new'
      axios.get(url).then(res=>{
         console.log('会员新鲜事图片:',res.data.data)
         store.commit('updateNew',res.data.data)
       })
     },
    //  获取轮播图接口数据
     getSwipe(store) {
      const url='home/swipe'
      axios.get(url).then(res=>{
        console.log('轮播图信息:',res)
        store.commit('updateSwipe',res.data)
      })
    }
    },
  modules: {
  }
})
