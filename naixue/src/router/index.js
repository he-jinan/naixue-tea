import Vue from "vue";
import VueRouter from "vue-router";
import store from '../store'
import { Dialog } from 'vant'
Vue.use(VueRouter);
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
	if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
	return originalPush.call(this, location).catch(err => err)}
const routes = [
  {
    path: "/",
    redirect: "/home/index",
  },
  //会员新鲜事
  {
    path: '/news',
    name: 'News',
    component: () => import('../views/News.vue'),
  },
  //新鲜事详情
  {
    path: '/nd1',
    name: 'ND1',
    component: () => import('../views/newsDetail1.vue'),
  },
  {
    path: '/nd2',
    name: 'ND2',
    component: () => import('../views/newsDetail2.vue'),
  },
  {
    path: '/nd3',
    name: 'ND3',
    component: () => import('../views/newsDetail3.vue'),
  },
  {
    path: '/nd4',
    name: 'ND4',
    component: () => import('../views/newsDetail4.vue'),
  },
  //首页底部活动展示详情
  {
    path: '/td1',
    name: 'Tea',
    component: () => import('../views/Tea.vue'),
  },
  {
    path: '/td2',
    name: 'Balance',
    component: () => import('../views/Balance.vue'),
  },
  {
    path: '/td3',
    name: 'Like',
    component: () => import('../views/Like.vue'),
  },
  //底部导航栏
  {
    path: "/home",
    name: "Home",
    component: () => import("../views/Home.vue"),
    redirect: "/home/index",
    children: [
      {
        path: "index",
        name: "Index",
        component: () => import("../views/Index.vue"),
      },
      {
        path: "products",
        name: "Products",
        component: () => import("../views/Products.vue"),
      },
      {
        path: 'orders',
        name: 'Orders',
        component: () => import('../views/Orders.vue'),
        redirect:"/home/orders/now",
        children:[
          {
            path: 'now',
            name: 'Now',
          },
          {
            path: 'history',
            name: 'History',
          },
        ]
      },
      {
        path: 'me',
        name: 'Me',
        component: () => import('../views/Me.vue'),
      },
      // 个人信息
      {
        path: 'personal',
        name: 'Personal',
        component: () => import('../views/Personal.vue'),
      },
      // 密码修改
      {
        path: 'pwd',
        name: 'Password',
        component: () => import('../views/Password.vue'),
      },
      // 手机号修改
      {
        path: 'phone',
        name: 'Phone',
        component: () => import('../views/EditPhone.vue'),
      },
    ],
  },
  //搜索
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
  },
  // 注册
  {
    path: '/reg',
    name: 'Reg',
    component: () => import('../views/Reg.vue'),
  },
  //登录
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
  //OrderDetail 订单详情
  {
    path: '/od',
    name: 'OrderDetail',
    component: () => import('../views/OrderDetail.vue'),
  },
  // 商品详情
  {
    path: '/pd/:lid',
    name: 'ProductDetail',
    component: () => import('../views/ProductDetails.vue'),
    props:true,
  },
  //地址
  {
    path: '/addr',
    name: 'Addr',
    component: () => import('../views/Addr.vue'),
  },
// 定位
  {
    path: '/geoloc',
    name: 'Geoloc',
    component: () => import('../views/Geoloc.vue'),
  },
  //城市选择页面
  {
    path: '/city',
    name: 'City',
    component: () => import('../views/City.vue'),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
//路由的拦截
router.beforeEach((to, from, next) => {
  if(to.name=='Me' && store.state.user==null){
    Dialog.confirm({
      message: '点击确定即将跳转登录页~',
      title: '请您先登录！',
      theme: 'round-button',
      showCancelButton:'true',
    }).then(() => {
      next('/login')
    }).catch(() => {
      // on cancel
    });
    return
  }
  next()
})
export default router;
